#include <iostream>
#include <Windows.h>
#include <conio.h>
#include <random>
using namespace std;
void createArray() {
	random_device rd;
	uniform_int_distribution<int> gen(100, 500);
	uniform_int_distribution<long long int> gn(1, 10000);
	int num = gen(rd);
	long long int* array = new long long int[num];
	for (int i = 0; i < num; ++i) {
		array[i] = gn(rd);
	}
	for (int j = 0; j < num; ++j) {
		cout << array[j] << endl;
	}
	cout << "El tamano del arreglo es " << num;
	delete[] array;
}
int main() {
	cout << "Haga clic para crear el arreglo..." << endl;
	_getch();
	createArray();
	return 0;
}